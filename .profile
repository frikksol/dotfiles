export QT_QPA_PLATFORMTHEME="qt5ct"
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"

# Default Programs
export TERMINAL="xfce4-terminal"
export EDITOR="nvim"
export BROWSER="brave"
export FILES="thunar"
export CALCULATOR="galculator"
export READER="zathura"
export SETTINGS="xfce4-settings-manager"

# This is a hack for java applications that needs text input
export _JAVA_AWT_WM_NONREPARTENTING=1
export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true'
wmname compiz
