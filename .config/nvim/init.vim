" _____     _ _    _            _
"|  ___| __(_) | _| | __ __   _(_)_ __ ___
"| |_ | '__| | |/ / |/ / \ \ / / | '_ ` _ \
"|  _|| |  | |   <|   <   \ V /| | | | | | |
"|_|  |_|  |_|_|\_\_|\_\   \_/ |_|_| |_| |_|

let mapleader =" "
if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
		\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()
Plug 'terryma/vim-multiple-cursors'
Plug 'preservim/nerdtree'
Plug 'itchyny/lightline.vim'
Plug 'frazrepo/vim-rainbow'
Plug 'airblade/vim-gitgutter'
Plug 'junegunn/fzf', { 'do': './install --bin' }
Plug 'junegunn/fzf.vim'
Plug 'ycm-core/YouCompleteMe'
Plug 'tpope/vim-fugitive'
Plug 'ntpeters/vim-better-whitespace'
Plug 'leafOfTree/vim-vue-plugin'
Plug 'nvie/vim-flake8'
Plug 'vimwiki/vimwiki'
call plug#end()


" Basic stuff
	set nocompatible
	filetype plugin on
	syntax on
	colorscheme frikk-theme
	set encoding=utf-8
	set number relativenumber
	set autoread
	set incsearch
	set hlsearch
	nnoremap <Esc><Esc> :noh<CR>:ccl<CR>
" Disables automatic commenting on newline:
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
" Color scheme modifications
	hi ErrorMsg term=standout cterm=bold ctermfg=0 ctermbg=1
	hi IncSearch term=reverse ctermfg=0 ctermbg=10
	hi Search term=reverse ctermfg=0 ctermbg=12
	hi DiffAdd term=bold ctermfg=0 ctermbg=4
	hi DiffChange term=bold ctermfg=0 ctermbg=5
	hi DiffDelete term=bold cterm=bold ctermfg=0 ctermbg=6
	hi DiffText term=reverse cterm=bold ctermfg=0 ctermbg=1
	hi SpellBad term=reverse ctermfg=0 ctermbg=9
	hi SpellCap term=reverse ctermfg=0 ctermbg=12
	hi SpellRare term=reverse ctermfg=0 ctermbg=13
	hi SpellLocal term=underline ctermfg=0 ctermbg=14
	hi ColorColumn term=reverse ctermfg=0 ctermbg=1
	hi Error term=reverse ctermfg=0 ctermbg=1
" Enable autocompletion
	set wildmode=longest,list,full
" vim-multiple-cursors subleader: m
	let g:multi_cursor_use_default_mapping=0
	let g:multi_cursor_start_word_key      = '<leader>msw'
	let g:multi_cursor_select_all_word_key = '<leader>maw'
	let g:multi_cursor_start_key           = '<leader>ms'
	let g:multi_cursor_select_all_key      = '<leader>ma'
	let g:multi_cursor_next_key            = '<leader>mn'
	let g:multi_cursor_prev_key            = '<leader>mp'
	let g:multi_cursor_skip_key            = '<leader>msk'
	let g:multi_cursor_quit_key            = '<Esc>'
" nerdtree subleader: t
	map <leader>ff :NERDTreeToggle<CR>
	"let g:rainbow_ctermfgs = ['lightblue', 'lightgreen', 'yellow', 'red', 'magenta']
	let g:rainbow_ctermfgs = [1, 2, 3, 4, 5]
" lightline
	set laststatus=2
	function! GetYcmInfo()
		let warnings = youcompleteme#GetWarningCount()
		let errors = youcompleteme#GetErrorCount()
		return 'W: '.warnings.'  E: '.errors
	endfunction

	let g:lightline = {
		\ 'colorscheme': 'seoul256',
		\ 'active': {
		\	'left': [['mode', 'paste'],
		\		['git', 'filename', 'modified']],
		\	'right':[['lineinfo'],
		\		['percent'],
		\		['readonly', 'filetype'],
		\		['ycminfo']],
		\ },
		\ 'component_function': {
		\ 	'git': 'FugitiveHead',
		\	'ycminfo': 'GetYcmInfo',
		\ },
		\ }
" vim-rainbow
	au FileType c,cpp,objc,objcpp,java,kotlin call rainbow#load()
" gitgutter subleader g
	let g:gitgutter_git_executable = '/usr/bin/git'
	nmap <leader>gn <Plug>(GitGutterNextHunk)
	nmap <leader>gp <Plug>(GitGutterPrevHunk)
"	nmap ghs <Plug>(GitGutterStageHunk)

"	nmap ghu <Plug>(GitGutterUndoHunk)
"	nmap ghp <Plug>(GitGutterPreviewHunk)
"	omap ih <Plug>(GitGutterTextObjectInnerPending)
"	omap ah <Plug>(GitGutterTextObjectOuterPending)
"	xmap ih <Plug>(GitGutterTextObjectInnerVisual)
"	xmap ah <Plug>(GitGutterTextObjectOuterVisual)
" fzf
	map <leader>gs :GitFiles?<CR>
	map <leader>fs :FZF<CR>
	map <leader>fa :Ag<CR>
	map <leader>fb :Buffers<CR>
" YouCompleteMe
	map <leader>cinc :YcmCompleter GoToInclude<CR>
	map <leader>cd :YcmCompleter GoToDeclaration<CR>
	map <leader>ci :YcmCompleter GoToDefinition<CR>
	map <leader>ct :YcmCompleter GoTo<CR>
	map <leader>cimp :YcmCompleter GoToImplementation<CR>
	map <leader>ctype :YcmCompleter GetType<CR>

" vim-better-whitespace
	let g:better_whitespace_enabled=1
	let g:strip_whitespace_on_save=1

" flake8
	let g:flake8_show_in_gutter=1
	let g:flake8_show_in_file=0
	autocmd BufWritePost *.py call flake8#Flake8()

" vimwiki
	let g:vimwiki_list = [{'path': '~/Documents/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]

" save file commands
	autocmd BufWritePost $HOME/.config/bspwm/bspwmrc !$HOME/.config/bspwm/bspwmrc &
	let g:flake8_quickfix_height=20
