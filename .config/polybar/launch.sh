#!/usr/bin/env sh

## Add this to your wm startup file.

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch bar1 and bar2
polybar -c ~/.config/polybar/config.ini main &

# restart trayer
killall -q trayer
sleep 3
trayer --edge top --align right --height 27 --iconspacing 20 --widthtype request --transparent true --tint 0x1F1F1F --alpha 0 --margin 100 --padding 10 --monitor primary &

